package com.inin.exampleService.models;

import com.inin.data.domain.annotation.Key;
import com.inin.data.domain.annotation.ModifiedTime;

import java.util.Date;

public abstract class BaseModelEntity {

    private Date dateCreated;

    @ModifiedTime
    private Date dateModified;

    private Integer version;

    @Key(rangeKey = true)
    private String id;

    @Key(hashKey = true)
    private String organizationId;

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
}
