package com.inin.exampleService.models;

import java.util.Date;
import java.util.UUID;

public class ExampleModel extends BaseModelEntity {
    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public ExampleModel() {
        this.setDateCreated(new Date());
        this.setId(UUID.randomUUID().toString());
    }
}
