package com.inin.exampleService.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.inin.schema.exampleService.api.ErrorInfo;
import com.inin.schema.exampleService.api.UserParam;
import com.inin.app.management.featuretoggle.OrgAwareFeatureEvaluator;
import com.inin.exampleService.exceptions.ExampleServiceApiException;
import com.inin.exampleService.exceptions.InternalServerError;
import com.inin.exampleService.exceptions.RestError;
import com.inin.rest.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.List;

public class BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    private OrgAwareFeatureEvaluator orgAwareFeatureEvaluator;

    public static ErrorInfo convertRestError(ExampleServiceApiException exception) {
        com.inin.rest.exception.RestError restError = exception.getRestError();

        if (restError != null) {
            return new ErrorInfo()
                    .withUserMessage(exception.getUserMessage())
                    .withUserParams(exception.getUserParams())
                    .withUserParamsMessage(exception.getMessage())
                    .withStatus(restError.getStatus())
                    .withErrorCode(restError.getCode().toString());
        } else {
            return new ErrorInfo().withUserMessage(exception.getMessage());
        }
    }

    public static ErrorInfo convertValidationError(MethodArgumentNotValidException ex) {

        return new ErrorInfo()
                .withUserMessage(String.format(convertErrorsToString(ex.getBindingResult())))
                .withStatus(HttpStatus.BAD_REQUEST.value());
    }

    public static String convertErrorsToString(Errors errors) {

        List<ObjectError> objectErrors = errors.getAllErrors();
        StringBuilder sb = new StringBuilder();

        // get all the errors
        for (ObjectError objectError : objectErrors) {
            sb.append(objectError.getDefaultMessage() + "\n");
        }

        return sb.toString();
    }

    public static ErrorInfo convertException(Throwable ex, HttpStatus status, RestError restError) {
        return new ErrorInfo()
                .withUserMessage(String.format("An error has occurred, please contact support. Message: %s", ex.getMessage()))
                .withUserParamsMessage("An error has occurred, please contact support. Message {message}")
                .withUserParams(Arrays.asList(new UserParam().withKey("message").withValue(ex.getMessage())))
                .withStatus(status.value())
                .withErrorCode(restError.toString());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ApiIgnore
    public ErrorInfo handleRunTimeException(RuntimeException ex) {
        LOGGER.error("handleRunTimeException exception", ex);
        return BaseController.convertException(ex, HttpStatus.INTERNAL_SERVER_ERROR, RestError.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ApiIgnore
    public ErrorInfo handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        LOGGER.error("handleHttpMessageNotReadableException Exception", ex);
        return BaseController.convertException(ex, HttpStatus.BAD_REQUEST, RestError.BAD_REQUEST);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ResponseBody
    @ApiIgnore
    public ErrorInfo handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex) {
        LOGGER.error("handleHttpRequestMethodNotSupportedException Exception", ex);
        return BaseController.convertException(ex, HttpStatus.METHOD_NOT_ALLOWED, RestError.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ApiIgnore
    public ErrorInfo handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        LOGGER.error("handleMethodArgumentNotValidException Exception", ex);
        return BaseController.convertValidationError(ex);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ApiIgnore
    public ErrorInfo handleHttpClientErrorException(HttpClientErrorException ex) {
        LOGGER.error("HttpClientErrorException Exception", ex);
        return BaseController.convertException(ex, ex.getStatusCode(), RestError.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    @ApiIgnore
    public ErrorInfo handleResourceNotFoundException(ResourceNotFoundException ex) {
        LOGGER.error("handleResourceNotFoundException Exception", ex);
        return BaseController.convertException(ex, HttpStatus.NOT_FOUND, RestError.RESOURCE_NOT_FOUND);
    }

    @ExceptionHandler(InternalServerError.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorInfo handleInternalServerError(InternalServerError exception) throws JsonProcessingException {
        LOGGER.error("handleInternalServerError exception", exception);
        return BaseController.convertRestError(exception);
    }
}


