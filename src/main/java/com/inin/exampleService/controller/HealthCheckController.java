package com.inin.exampleService.controller;

import com.inin.app.management.health.HealthCheckRegistry;
import com.inin.contracts.app.management.HealthCheckPayload;
import com.inin.exampleService.config.ExampleServiceConfigKey;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/health")
public class HealthCheckController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HealthCheckController.class);

    private static final int HEALTH_CHECK_SHUTDOWN_TIME_SECONDS = 5;

    private final ExecutorService healthCheckExecutor = Executors.newCachedThreadPool();

    @PostConstruct
    private void initialize() {
        HealthCheckRegistry.INSTANCE.initialize(HealthCheckController.class, ExampleServiceConfigKey.ApplicationId.key());
    }

    @ResponseBody
    @RequestMapping(value = "/check", method = {RequestMethod.GET})
    @ApiOperation(value = "Perform a health check")
    public ResponseEntity<HealthCheckPayload> checkHealth() {
        HealthCheckPayload result = HealthCheckRegistry.INSTANCE.runHealthChecks(healthCheckExecutor);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getHttpStatus()));
    }

    @PreDestroy
    public void shutdown() {
        if (healthCheckExecutor != null) {
            try {
                this.healthCheckExecutor.shutdown();
                this.healthCheckExecutor.awaitTermination(HEALTH_CHECK_SHUTDOWN_TIME_SECONDS, TimeUnit.SECONDS);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                try {
                    this.healthCheckExecutor.shutdownNow();
                } catch (Exception exUhOh) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }
    }
}
