package com.inin.exampleService.exceptions;

public enum RestError {

    /* General Errors */
    BAD_REQUEST,
    RESOURCE_NOT_FOUND,
    RESOURCE_CANNOT_CREATE,
    INTERNAL_SERVER_ERROR,
    METHOD_NOT_ALLOWED,
    FORBIDDEN,
    BAD_GATEWAY
}
