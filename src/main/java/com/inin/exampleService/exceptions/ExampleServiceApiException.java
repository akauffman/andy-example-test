package com.inin.exampleService.exceptions;

import com.inin.rest.exception.ApiException;
import org.apache.commons.lang3.text.StrSubstitutor;
import com.inin.schema.exampleService.api.ErrorInfo;
import com.inin.schema.exampleService.api.UserParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExampleServiceApiException extends ApiException {
    private static final String PREFIX = "{";
    private static final String SUFFIX = "}";

    protected List<UserParam> userParams = new ArrayList<>();

    public ExampleServiceApiException(int status, Enum<?> code, String message, String details, String documentationUri) {
        super(status, code, message, details, documentationUri);
    }

    public List<UserParam> getUserParams() {
        return userParams;
    }

    public ExampleServiceApiException withUserParams(List<UserParam> userParams) {
        this.userParams = userParams;
        return this;
    }

    public ExampleServiceApiException withUserParam(UserParam userParam) {
        this.userParams.add(userParam);
        return this;
    }

    protected static String format(String message, Map<String, String> data) {
        return StrSubstitutor.replace(message, data, PREFIX, SUFFIX);
    }

    protected Map<String, String> toMap(List<UserParam> userParamList) {
        Map<String, String> userParamMap = new HashMap<>();
        for (UserParam userParam : userParamList) {
            userParamMap.put(userParam.getKey(), userParam.getValue());
        }

        return userParamMap;
    }

    public String getUserMessage() {
        return format(getRestError().getMessage(),
                toMap(getUserParams()));
    }

    @Override
    public String toString() {
        return getUserMessage();
    }
}
