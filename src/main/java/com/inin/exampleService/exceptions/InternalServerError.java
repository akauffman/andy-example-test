package com.inin.exampleService.exceptions;


import org.springframework.http.HttpStatus;
import com.inin.schema.exampleService.api.UserParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerError extends ExampleServiceApiException {
    private static final int STATUS_CODE = 500;
    private static final long serialVersionUID = -2046780454548020540L;
    private static final String MESSAGE = "Internal Server Error: {errorMessage}";
    private static final String PARAMETER = "errorMessage";

    public InternalServerError(final String parameter) {
        this(RestError.INTERNAL_SERVER_ERROR, parameter);
    }

    public InternalServerError(Enum<?> code, String parameter) {
        this(code, MESSAGE, null, null);
        userParams.add(new UserParam().withKey(PARAMETER).withValue(parameter));
    }

    public InternalServerError(final Enum<?> code, String message, String details, String documentationUrl) {
        super(STATUS_CODE, code, message, details, documentationUrl);
    }
}
