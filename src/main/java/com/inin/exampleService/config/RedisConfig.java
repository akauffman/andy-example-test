package com.inin.exampleService.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisConfig {

    protected static final Logger LOGGER = LoggerFactory.getLogger(RedisConfig.class);

    @Bean
    @Qualifier("Integer")
    RedisTemplate<String, Integer> redisIntegerTemplate(ExampleServiceProperties exampleServiceProperties){
        RedisTemplate<String, Integer> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(jedisConnectionFactory(exampleServiceProperties));
        redisTemplate.setKeySerializer(new StringRedisSerializer());

        redisTemplate.setValueSerializer(new GenericToStringSerializer<Integer>(Integer.class));
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new GenericToStringSerializer<Integer>(Integer.class));
        return redisTemplate;
    }

    @Bean
    JedisConnectionFactory jedisConnectionFactory(ExampleServiceProperties exampleServiceProperties) {
        JedisConnectionFactory factory = new JedisConnectionFactory();
        factory.setHostName(exampleServiceProperties.getRedisServer());
        factory.setPort(exampleServiceProperties.getRedisPort());
        factory.setUsePool(true);

        /**
         * These settings are completely arbitrary.  The idea is
         * that we want to keep at least 1 connection all the time,
         * and fail fast if our pool runs out of connections.
         */
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(250); // max default client count for the server is 10K
        poolConfig.setMaxIdle(20);
        poolConfig.setMinIdle(1);
        poolConfig.setTestOnBorrow(false);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setBlockWhenExhausted(false);

        factory.setPoolConfig(poolConfig);
        return factory;
    }

    @Bean
    @Qualifier("String")
    org.springframework.data.redis.core.StringRedisTemplate redisStringTemplate(ExampleServiceProperties exampleServiceProperties) {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(jedisConnectionFactory(exampleServiceProperties));
        return redisTemplate;
    }
}
