package com.inin.exampleService.config;

import com.inin.data.aws.models.AwsEntityManager;
import com.inin.data.aws.util.AwsEntityUtil;
import com.inin.data.domain.EntityManager;
import com.inin.data.domain.annotation.DbType;
import com.inin.exampleService.models.ExampleModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class ExampleEntityManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExampleEntityManager.class);
    private static final String INTEGRATION_TEST_DYNAMO_ENDPOINT = "pc.it.dynamodb.endpoint";

    private final ExampleServiceProperties exampleServiceProperties;
    private final String globalPrefix;

    private EntityManager entityManager;

    @Autowired
    public ExampleEntityManager(final ExampleServiceProperties exampleServiceProperties) {
        this.exampleServiceProperties = exampleServiceProperties;
        this.globalPrefix = exampleServiceProperties.getGlobalPrefix();
    }

    @PostConstruct
    private final void init() {
        setupEntityManager();
        registerModels();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PreDestroy
    public final void shutdown() {
        entityManager.shutdown();
    }

    private void registerModels() {
        entityManager.registerModel(ExampleModel.class, DbType.DynamoDB);
        entityManager.createRegisteredTables();
    }

    private final void setupEntityManager() {
        String region = exampleServiceProperties.getRegion();

        try {
            final String endpoint = StringUtils.isBlank(System.getProperty(INTEGRATION_TEST_DYNAMO_ENDPOINT)) ?
                    AwsEntityUtil.getDynamoEndpointForRegion(region) :
                    System.getProperty(INTEGRATION_TEST_DYNAMO_ENDPOINT);

            this.entityManager = new AwsEntityManager();


            entityManager.setGlobalPrefix(globalPrefix);
            entityManager.setDbEndpoint(DbType.DynamoDB, endpoint);
        } catch (Exception ex) {
            LOGGER.error("An error occurred when setting up the entity manager", ex);
        }
    }
}
