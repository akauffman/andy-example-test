package com.inin.exampleService.config;

import com.amazonaws.services.elasticache.model.Endpoint;
import com.inin.app.management.config.GlobalConfigKey;
import com.inin.app.management.properties.PropertyRegistry;
import com.inin.app.management.properties.PropertyService;
import com.inin.aws.services.elasticache.ClusterUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Dynamic Properties for Example Service
 */
@Component
public class ExampleServiceProperties {
    private static final String ORGS_PATH = "/organizations";
    private static final String REDIS_CLUSTER_NAME = "exampleService-redis";
    private static final int DEFAULT_REDIS_PORT = 6379;
    public static final String DYNAMIC_DYNAMODB_PROPERTY = "pc.it.dynamodb.endpoint";
    public static final String FEATURE_NAME = "exampleService";
    public static final String DYNAMIC_SQS_PROPERTY = "pc.it.sqs.endpoint";
    public static final String DYNAMIC_S3_PROPERTY = "pc.it.s3.endpoint";


    protected static final Logger LOGGER = LoggerFactory.getLogger(ExampleServiceProperties.class);
    private final PropertyRegistry propertyRegistry;
    /*
     * Defaults
     */
    private static class Defaults {
        public static final boolean USE_SSL = false; //Use SSL for connections to other services
    }

    private PropertyService propertyService;

    @Autowired
    public ExampleServiceProperties(PropertyService propertyService) {
        this.propertyService = propertyService;
        this.propertyRegistry = propertyService.getPropertyRegistry();
    }

    public String getGlobalPrefix() {
        return propertyRegistry.getString(ExampleServiceConfigKey.GlobalPrefix.key(), "");
    }

    public String getRegion() {
        return propertyRegistry.getString(GlobalConfigKey.Region.key());
    }

    public boolean getUseSSL() {
        return propertyRegistry.getBoolean(ExampleServiceConfigKey.UseSSL.key(), Defaults.USE_SSL);
    }

    public String getZookeeperList() {
        return this.propertyService.getString(GlobalConfigKey.ZookeeperList.key());
    }

    public String getKafkaBrokerList() {
        return this.propertyService.getString(GlobalConfigKey.KafkaBrokerList.key());
    }

    private String getUriPrefix() {
        return this.getUseSSL() ? "https://" : "http://";
    }

    public boolean isIntegrationTestEnvironment() {
        return StringUtils.contains(
                System.getProperty("pc.platform.property.file"),
                "src/test/resources/example-service.properties");
    }

    private String getServiceUriScheme() {
        return this.getUseSSL() ? "https://" : "http://";
    }

    public String getRedisServer() {
        String redisServer = propertyService.getPropertyRegistry().getString(ExampleServiceConfigKey.RedisServer.key());
        if(org.apache.commons.lang.StringUtils.isBlank(redisServer)) {
            List<Endpoint> endpoints = ClusterUtils.getRedisEndpoints(REDIS_CLUSTER_NAME);
            if(endpoints != null && endpoints.size() > 0) {
                LOGGER.info("Using redis server at {} from cluster name {}", endpoints.get(0).getAddress(), REDIS_CLUSTER_NAME);
                redisServer = endpoints.get(0).getAddress();
            } else {
                LOGGER.error("No redis endpoint found with cluster name {}", REDIS_CLUSTER_NAME);
            }
        } else {
            LOGGER.info("Using redis server at {} from property {}", redisServer, ExampleServiceConfigKey.RedisServer.key());
        }
        return redisServer;
    }

    public int getRedisPort() {
        return propertyService.getPropertyRegistry().getInteger(ExampleServiceConfigKey.RedisPort.key(),
                DEFAULT_REDIS_PORT);
    }
}

