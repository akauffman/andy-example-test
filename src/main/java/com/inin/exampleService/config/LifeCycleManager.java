package com.inin.exampleService.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

@Component
public class LifeCycleManager implements ApplicationListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LifeCycleManager.class);

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        if (applicationEvent instanceof EmbeddedServletContainerInitializedEvent) {
            LOGGER.info("========================   Example Service Application Started   ========================");
            onApplicationStarted();
        } else if (applicationEvent instanceof ContextClosedEvent) {
            LOGGER.info("========================   Example Service Application Stopped   ========================");
            onApplicationStopped();
        } else {
            LOGGER.trace("Ignoring application event: <{}>", applicationEvent);
        }
    }

    protected void onApplicationStarted() {
        //do something
    }

    protected void onApplicationStopped() {
        // do something
    }
}
