package com.inin.exampleService.config;

import com.codahale.metrics.servlets.MetricsServlet;
import com.google.common.collect.ImmutableMap;
import com.inin.app.management.config.AppAdminConfig;
import com.inin.app.management.core.AppAdminService;
import com.inin.app.management.properties.PropertyService;
import com.inin.app.management.servlet.AdminAppServlet;
import com.inin.management.spring.servlet.MetricsListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextListener;

@Configuration
public class WebAppInitializer implements ServletContextAware {
    protected static final Logger LOGGER = LoggerFactory.getLogger(WebAppInitializer.class);

    @Autowired
    private AppAdminService appAdminService;
    @Autowired
    private AppAdminConfig appAdminConfig;

    @Bean
    public ServletRegistrationBean admin() {
        return new ServletRegistrationBean(new AdminAppServlet(appAdminService), "/admin/*");
    }

    @Bean
    public ServletContextListener metricsListener() {
        return new MetricsListener(appAdminConfig.getMetricsConfig().getRegistry());
    }

    @Bean
    public ServletRegistrationBean metricsServlet() {
        com.inin.app.management.config.MetricsConfig cfg = appAdminConfig.getMetricsConfig();
        MetricsServlet servlet = new MetricsServlet(cfg.getRegistry());
        ServletRegistrationBean reg = new ServletRegistrationBean(servlet, "/metrics/*");
        reg.setInitParameters(new ImmutableMap.Builder<String, String>()
                .put(MetricsServlet.RATE_UNIT, cfg.getRateUnit())
                .put(MetricsServlet.DURATION_UNIT, cfg.getDurationUnit())
                .put(MetricsServlet.SHOW_SAMPLES, Boolean.toString(cfg.getShowSamples()))
                .build());
        return reg;
    }

    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
        this.servletContext.setAttribute(MetricsServlet.METRICS_REGISTRY, appAdminConfig.getMetricsConfig().getRegistry());

        FilterRegistration.Dynamic encodingFilter = servletContext.addFilter("encoding-filter", new CharacterEncodingFilter());
        encodingFilter.setInitParameter("encoding", "UTF-8");
        encodingFilter.setInitParameter("forceEncoding", "true");
        encodingFilter.addMappingForUrlPatterns(null, false, "/*");// middle 'false' makes sure it is first filter in the chain

        LOGGER.info("Initializing properties with environment = {}", PropertyService.getEnvironment());
    }
}