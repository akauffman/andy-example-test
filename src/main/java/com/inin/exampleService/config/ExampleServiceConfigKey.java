package com.inin.exampleService.config;

import com.inin.app.management.config.ConfigKey;

public enum ExampleServiceConfigKey implements ConfigKey {
    ApplicationId("exampleService"),
    GlobalPrefix("datasource.global.prefix"),
    UseSSL("useSSL"),
    RedisServer("redis.server"),
    RedisPort("redis.port");

    private final String configKey;

    ExampleServiceConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String key() {
        return configKey;
    }
}
