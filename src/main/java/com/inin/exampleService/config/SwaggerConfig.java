package com.inin.exampleService.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.JarURLConnection;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurerAdapter {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_12)
                .apiInfo(apiInfo())
                .select()
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Example Service API")
                .description("Example Service description.")
                .termsOfServiceUrl("https://bitbucket.org/inindca/example-service-2016")
                .contact("example@foo.com")
                .license("©2015 Interactive Intelligence; all rights reserved.")
                .version(readVersionInfoInManifest())
                .build();
    }

    private String readVersionInfoInManifest() {
        URL res = SwaggerConfig.class.getResource(SwaggerConfig.class.getSimpleName() + ".class");

        try {
            JarURLConnection e = (JarURLConnection) res.openConnection();
            Manifest mn = e.getManifest();
            Attributes attr = mn.getMainAttributes();

            return attr.getValue("Implementation-Version");
        } catch (Exception ignored) {
            return "1.0";
        }
    }
}