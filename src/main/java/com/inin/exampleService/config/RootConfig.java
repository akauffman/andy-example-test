package com.inin.exampleService.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inin.app.management.featuretoggle.OrgAwareFeatureEvaluator;
import com.inin.app.management.properties.PropertyService;
import com.inin.data.aws.dynamodb.util.DynamoDbDataStore;
import com.inin.data.aws.dynamodb.util.DynamoDbDataStoreImpl;
import com.inin.integration.aws.messaging.QueueFactory;
import com.inin.integration.aws.messaging.SqsQueueFactory;
import com.inin.integration.core.repository.IschemaProvider;
import com.inin.integration.core.repository.S3SchemaRepository;
import com.inin.management.admin.ELBMonitor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PreDestroy;

@Configuration
public class RootConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(RootConfig.class);
    private ELBMonitor elbMonitor = null;

    @PreDestroy
    public void destroy() {
        if (elbMonitor != null) {
            try {
                this.elbMonitor.stop();
            } catch (Exception e) {
                LOGGER.warn("Unexpected error while shutting down the elbmonitor", e);
            }
        }
    }

    @Bean
    @Primary
    ObjectMapper getObjectMapper(){
        return SharedObjectFactory.getObjectMapper();
    }

    @Bean
    ExampleServiceProperties exampleServiceProperties(final PropertyService propertyService) {
        return new ExampleServiceProperties(propertyService);
    }

    @Bean
    OrgAwareFeatureEvaluator orgAwareFeatureEvaluator(){
        return new OrgAwareFeatureEvaluator();
    }

    @Bean
    public DynamoDbDataStore getDbDataStore() {
        final AmazonDynamoDBClient client = Regions.getCurrentRegion() != null ?
                                            Regions.getCurrentRegion().createClient(AmazonDynamoDBClient.class,
                                                                                    new DefaultAWSCredentialsProviderChain(),
                                                                                    new ClientConfiguration()) :
                                            new AmazonDynamoDBClient();

        final String endPoint = System.getProperty(ExampleServiceProperties.DYNAMIC_DYNAMODB_PROPERTY);
        if (StringUtils.isNotBlank(endPoint)) {
            LOGGER.info("Using custom DynamoDb Endpoint: <{}>", endPoint);
            client.setEndpoint(endPoint);
        }

        return new DynamoDbDataStoreImpl(client, "");
    }

    @Bean
    public AmazonDynamoDBClient getAmazonDynamoDBClient() {
        final AmazonDynamoDBClient client = Regions.getCurrentRegion() != null ?
                                            Regions.getCurrentRegion().createClient(AmazonDynamoDBClient.class,
                                                                                    new DefaultAWSCredentialsProviderChain(),
                                                                                    new ClientConfiguration()) :
                                            new AmazonDynamoDBClient();

        final String endPoint = System.getProperty(ExampleServiceProperties.DYNAMIC_DYNAMODB_PROPERTY);
        if (StringUtils.isNotBlank(endPoint)) {
            LOGGER.info("Using custom DynamoDb Endpoint: <{}>", endPoint);
            client.setEndpoint(endPoint);
        }
        return client;
    }

    @Bean
    @Primary
    public AmazonS3Client getS3Client() {
        final AmazonS3Client client = Regions.getCurrentRegion() != null ?
                                      Regions.getCurrentRegion().createClient(AmazonS3Client.class,
                                                                              new DefaultAWSCredentialsProviderChain(),
                                                                              new ClientConfiguration()) :
                                      new AmazonS3Client();

        final String customEndpoint = System.getProperty(ExampleServiceProperties.DYNAMIC_S3_PROPERTY);
        if (!StringUtils.isBlank(customEndpoint)) {
            LOGGER.info("Using custom S3 endpoint: <{}>", customEndpoint);
            client.setEndpoint(customEndpoint);
        }
        return client;
    }

    @Bean
    public IschemaProvider schemaProvider() {
        return S3SchemaRepository.getInstance();
    }

    @Bean
    public AmazonSQSClient getSQSClient() {
        final AmazonSQSClient client = Regions.getCurrentRegion() != null ?
                Regions.getCurrentRegion().createClient(AmazonSQSClient.class,
                        new DefaultAWSCredentialsProviderChain(),
                        new ClientConfiguration()) :
                new AmazonSQSClient();

        final String customEndpoint = System.getProperty(ExampleServiceProperties.DYNAMIC_SQS_PROPERTY);
        if (!StringUtils.isBlank(customEndpoint)) {
            LOGGER.info("Using custom SQS endpoint: <{}>", customEndpoint);
            client.setEndpoint(customEndpoint);
        }
        return client;
    }


    @Bean
    public QueueFactory getSqsQueueFactory(AmazonSQSClient sqsClient) {
        return new SqsQueueFactory(sqsClient);
    }
}
