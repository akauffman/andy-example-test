package com.inin.exampleService.config;

import com.inin.logging.ContextLogger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;

/**
 * Helper class for using SL4J mapped diagnostics contexts in try with resource statements.
 */
public class Context extends ContextLogger {

    public static final String CORRELATION_ID = "ININ-Correlation-Id";
    public static final String USER_ID = "ININ-User-Id";
    public static final String ORGANIZATION_ID_HEADER = "ININ-Organization-Id";
    public static final String REQUEST_ID = "X-Request-Id";

    public Context() {
        super();
    }

    @Override
    public Context withConversation(String id) {
        return (Context) with(Key.Conversation, id);
    }

    @Override
    public Context withUser(String id) {
        return (Context) with(Key.UserId, id);
    }

    @Override
    public Context withOrganization(String id) {
        return (Context) with(Key.Organization, id);
    }

    @Override
    public Context withConnection(String id) {
        return (Context) with(Key.Connection, id);
    }

    @Override
    public Context withCorrelationId(String id) {
        return (Context) with(Key.CorrelationId, id);
    }

    public String getOrganizationId() {
        return this.get(Key.Organization);
    }

    public String getUserId() {
        return this.get(Key.UserId);
    }

    @Override
    public String getCorrelationId() {
        return this.get(Key.CorrelationId);
    }

    public String getRemoteIp() {
        return this.get(Key.RemoteHost);
    }

    public String getSessionId() {
        return this.get(Key.SessionId);
    }

    public String getUserAgent() {
        return this.get(Key.UserAgent);
    }

    public void addToRequest(HttpServletRequest request) {
        request.setAttribute("com.inin.context", this);
    }

    public static Context fromRequest(HttpServletRequest request) {
        Context result = (Context)request.getAttribute("com.inin.context");
        if (result == null) {
            result = new Context()
                    .withCorrelationId(request.getHeader(CORRELATION_ID))
                    .withOrganization(request.getHeader(ORGANIZATION_ID_HEADER))
                    .withUser(request.getHeader(Context.USER_ID));

            result.withHttpMethod(request.getMethod());
            result.withRequestUri(request.getRequestURI());
            result.withRequestQueryString(request.getQueryString());
            result.withUserAgent(request.getHeader(HttpHeaders.USER_AGENT));

            result.addToRequest(request);
        }
        return result;
    }
}
