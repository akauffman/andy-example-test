package com.inin.exampleService.config;

import com.codahale.metrics.*;
import com.codahale.metrics.servlet.InstrumentedFilter;
import com.inin.app.management.config.AppAdminConfig;
import com.inin.app.management.config.TargetPlatform;
import com.inin.app.management.core.AppAdminService;
import com.inin.app.management.core.NodeDiscovery;
import com.inin.app.management.health.HealthCheckRegistry;
import com.inin.app.management.initialization.AppInitializerRegistry;
import com.inin.app.management.initialization.ResourceConfigurer;
import com.inin.app.management.initialization.ResourceInitializationFactory;
import com.inin.app.management.properties.PropertyService;
import com.inin.data.dynamodb.DynamoDBUsageTracker;
import com.inin.management.admin.AwsAppAdminService;
import com.inin.management.healthcheck.DynamoDynamicPropertiesHealthCheck;
import com.inin.management.initialization.AwsResourceInitializationFactory;
import com.palominolabs.metrics.newrelic.AllDisabledMetricAttributeFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.Filter;
import java.io.IOException;
import java.util.*;

@Configuration
@EnableAspectJAutoProxy
@EnableAsync
public class WebConfig extends WebMvcConfigurerAdapter {
    private AppAdminConfig appAdminConfig;
    private AppAdminService appAdminService;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/error").setViewName("error");
    }

    @Bean
    public RestTemplate getRestTemplate() {

        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        configureMessageConverters(converters);
        restTemplate.setMessageConverters(converters);
        return restTemplate;
    }

    @Bean
    public PropertyService getPropertyService(AppAdminService appAdminService) throws IOException {
        return appAdminService.getPropertyService();
    }

    @Bean
    public ResourceInitializationFactory getResourceInitializationFactory(AppAdminConfig appAdminConfig) {
        return createInitializerAndBootstrap(appAdminConfig);
    }

    @Bean(destroyMethod = "shutdown")
    public AppAdminService getAppAdminService(AppAdminConfig appAdminConfig,
                                              ResourceInitializationFactory factory) {
        if (appAdminService == null) {

            appAdminService = new AwsAppAdminService(appAdminConfig);
            appAdminService.initialize();

            // run the resource initialization
            ResourceConfigurer resourceInitializers = factory.getResourceInitializers();
            resourceInitializers.setPropertyService(appAdminService.getPropertyService());

            NodeDiscovery nodeDiscovery = factory.discoverNodesByCurrentInstanceRole();
            appAdminService.setNodeDiscovery(nodeDiscovery);

            HealthCheckRegistry.INSTANCE.get()
                    .register("dynamicPropertiesHealthCheck",
                            DynamoDynamicPropertiesHealthCheck.newBuilder()
                                    .withPropertyService(appAdminService.getPropertyService())
                                    .withConfig(appAdminConfig.getHealthCheckConfig())
                                    .build());

            AppInitializerRegistry.INSTANCE.get().runInitializers();
        }

        return appAdminService;
    }

    private ResourceInitializationFactory createInitializerAndBootstrap(AppAdminConfig config) {
        ResourceInitializationFactory initializationFactory = null;

        if (config.getTargetPlatform().equals(TargetPlatform.AWS)) {
            initializationFactory = new AwsResourceInitializationFactory(config.getAppKey(), PropertyService.getEnvironment());
        }

        initializationFactory.createInitialBootstrap();
        return initializationFactory;
    }

    @Bean
    public AppAdminConfig getAppAdminConfig() {

        if (appAdminConfig == null) {
            Set<String> requiredProperties = new HashSet<>(Arrays.asList(
                    ExampleServiceConfigKey.GlobalPrefix.key()));

            MetricFilter metricFilter = (name, metric) -> name.contains("com.inin.exampleService");

            // We shouldn't have to do this, but due to stupid changes in App-Management which were intended
            // to avoid doing this, we have to do this instead of using withRole and withOwner which are now deprecated.
            DynamoDBUsageTracker.setRole(ExampleServiceConfigKey.ApplicationId.key());
            DynamoDBUsageTracker.setOwner("exampleServiceContact");

            appAdminConfig = new AppAdminConfig.Builder(PropertyService.getEnvironment(), ExampleServiceConfigKey.ApplicationId)
                    .withIncludeJmxMetrics(false)
                    .withIncludeLogBackMetrics(false)
                    .withShowMetricSamples(false)
                    .withNewRelicReporter(metricFilter, new AttrFilter())
                    .checkPropertiesHaveValue(requiredProperties)
                    .build();
        }
        return appAdminConfig;
    }

    @Bean
    public Filter characterEncodingFilter() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        return characterEncodingFilter;
    }

    @Bean
    public Filter instrumentedFilter() {
        return new InstrumentedFilter();
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter jacksonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jacksonHttpMessageConverter.setObjectMapper(SharedObjectFactory.getObjectMapper());
        converters.add(jacksonHttpMessageConverter);
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON)
                .mediaType("json", MediaType.APPLICATION_JSON)
                .mediaType("html", MediaType.TEXT_HTML);
    }

    private class AttrFilter extends AllDisabledMetricAttributeFilter {
        @Override
        public boolean recordGaugeValue(String name, Gauge metric) {
            return true;
        }

        @Override
        public boolean recordHistogramMedian(String name, Histogram metric) {
            return true;
        }

        @Override
        public boolean recordTimerMedian(String name, com.codahale.metrics.Timer metric) {
            return true;
        }

        @Override
        public boolean recordMeter1MinuteRate(String name, Meter metric) {
            return true;
        }

        @Override
        public boolean recordCounterCount(String name, Counter metric) {
            return true;
        }
    }
}
