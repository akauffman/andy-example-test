package com.inin.exampleService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
@ComponentScan(basePackages = {
        "com.inin.exampleService",
        "com.inin.spring.metrics",
        "com.inin.spring.storage",
        "com.inin.spring.rest.exception",
        "com.inin.app.management.featuretoggle"})
public class Server extends WebMvcAutoConfiguration {
    public static void main (String[] args) {
        new SpringApplication(Server.class).run(args);
    }
}
