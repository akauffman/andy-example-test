package com.inin.exampleService.validators.util;

import org.springframework.validation.Errors;

public class ValidationUtil {
    private static final String MUST_NOT_BE_NULL_MESSAGE = "The property value for '%s' cannot be empty";

    private static final String BASE_ERROR_CODE_PATTERN = "error.%s";
    private static final String COLUMN_ERROR_CODE_PATTERN = BASE_ERROR_CODE_PATTERN + "%s";

    public static void rejectNullColumnValue(String className, String columnName, Errors errors){
        errors.rejectValue(columnName, String.format(COLUMN_ERROR_CODE_PATTERN, className, columnName), String.format(MUST_NOT_BE_NULL_MESSAGE, columnName));
    }

    public static void rejectWithMessage(String className, String message, Errors errors){
        errors.reject(String.format(BASE_ERROR_CODE_PATTERN, className), message);
    }
}
