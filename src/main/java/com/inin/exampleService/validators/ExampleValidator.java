package com.inin.exampleService.validators;

import com.amazonaws.util.StringUtils;
import com.inin.exampleService.models.ExampleModel;
import com.inin.exampleService.validators.util.ValidationUtil;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ExampleValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return ExampleModel.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ExampleModel exampleModel = (ExampleModel)obj;

        if (StringUtils.isNullOrEmpty(exampleModel.getName())){
            ValidationUtil.rejectNullColumnValue(this.getClass().getName(), "name", errors);
        }

        if  (StringUtils.isNullOrEmpty(exampleModel.getId())){
            ValidationUtil.rejectNullColumnValue(this.getClass().getName(), "id", errors);
        }
    }
}
