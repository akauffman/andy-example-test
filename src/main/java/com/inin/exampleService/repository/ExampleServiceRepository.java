package com.inin.exampleService.repository;

import com.inin.data.domain.Query;
import com.inin.exampleService.config.ExampleEntityManager;
import com.inin.exampleService.models.ExampleModel;
import com.inin.framework.utils.PageUtils;
import com.inin.models.data.DeserializablePageImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class ExampleServiceRepository {

    private final ExampleEntityManager exampleEntityManager;

    @Autowired
    public ExampleServiceRepository(final ExampleEntityManager exampleEntityManager) {
        this.exampleEntityManager = exampleEntityManager;
    }

    public ExampleModel create(ExampleModel exampleModel) {
        exampleModel.setId(UUID.randomUUID().toString());
        exampleModel.setDateCreated(new Date());
        this.exampleEntityManager.getEntityManager().dao(ExampleModel.class).put(exampleModel);
        return exampleModel;
    }

    public void update(ExampleModel exampleModel) {
        this.exampleEntityManager.getEntityManager().dao(ExampleModel.class).put(exampleModel);
    }

    public void delete(String organizationId, String id) {
        this.exampleEntityManager.getEntityManager().dao(ExampleModel.class).delete(organizationId, id);
    }

    public ExampleModel get(String organizationId, String id) {
        return this.exampleEntityManager.getEntityManager().dao(ExampleModel.class).get(organizationId, id);
    }

    public DeserializablePageImpl<ExampleModel> getAll(String organizationId, int pageNumber, int pageSize,
                                                       String sortBy, Boolean sortAsc){
        Query<ExampleModel> query = this.exampleEntityManager.getEntityManager().query(ExampleModel.class).eq("organizationId", organizationId);

        List<ExampleModel> examples = new ArrayList<>();
        while (query.hasNext()) {
            examples.addAll(query.run());
        }
        return (DeserializablePageImpl) PageUtils.toPage(examples, pageNumber, pageSize);
    }

}