package com.inin.exampleService.service;


import com.inin.exampleService.models.ExampleModel;
import com.inin.exampleService.repository.ExampleServiceRepository;
import com.inin.models.data.DeserializablePageImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExampleService {

    private ExampleServiceRepository exampleServiceRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ExampleService.class);

    @Autowired
    public ExampleService(final ExampleServiceRepository exampleServiceRepository) {
        this.exampleServiceRepository = exampleServiceRepository;
    }

    public ExampleModel getExampleModel(String organizationId, String id) {
        try {
            LOGGER.debug("Getting example <{}> from repository", id);
            return exampleServiceRepository.get(organizationId, id);
        } catch (Exception ex) {
            LOGGER.error("Error getting example <{}>", id, ex);
            throw ex;
        }
    }

    public ExampleModel createExampleModel(String organizationId, ExampleModel exampleModel) {
        try {
            LOGGER.debug("Creating exampleModel <{}> for org {}.", exampleModel.getName(), organizationId);
            exampleModel.setOrganizationId(organizationId);
            return exampleServiceRepository.create(exampleModel);
        } catch (Exception ex) {
            LOGGER.error("An error occurred creating exampleModel", ex);
            throw ex;
        }
    }

    public ExampleModel updateExampleModel(String organizationId, ExampleModel exampleModel) {
        try {
            LOGGER.info("Updating exampleModel  {}.",  exampleModel.getId());
            exampleModel.setOrganizationId(organizationId);
            exampleServiceRepository.update(exampleModel);
        } catch (Exception ex) {
            LOGGER.error("An error occurred updating the Gistener with id {}", ex, exampleModel.getId());
            throw ex;
        }
        return exampleModel;
    }

    public String deleteExampleModel(String organizationId, String id) {
        try {
            LOGGER.info("Preparing to delete example model {}.", id);
            exampleServiceRepository.delete(organizationId, id);
        } catch (Exception ex) {
            LOGGER.error("An error occurred deleting the example with id {}", ex, id);
            throw ex;
        }
        return id;
    }

    public DeserializablePageImpl<ExampleModel> getAllExamples(String organizationId, int page, int pageSize, String sort, boolean sortAsc) {
        DeserializablePageImpl<ExampleModel> gisteners = null;
        try {
            LOGGER.info("Getting examples for org <{}>", organizationId);
            gisteners = exampleServiceRepository.getAll(organizationId, page, pageSize, sort, sortAsc);

        } catch (Exception ex) {
            LOGGER.error("An error occurred getting example models.", ex);
            throw ex;
        }
        return gisteners;
    }
}
