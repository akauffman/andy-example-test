package com.inin.exampleService.controller;

import com.inin.contracts.app.management.HealthCheckPayload;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

;

public class HealthCheckControllerTest {

    @Test
    public void testGetVersion() throws Exception {
        HealthCheckController controller = new HealthCheckController();
        ResponseEntity<HealthCheckPayload> resource = controller.checkHealth();
        controller.shutdown();
        Assert.assertNotNull(resource);
    }
}
