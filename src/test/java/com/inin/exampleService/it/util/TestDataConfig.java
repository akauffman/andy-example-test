package com.inin.exampleService.it.util;

import com.inin.exampleService.models.ExampleModel;

public class TestDataConfig {

    public static final String ORGANIZATION_ID = "55bc6be3-078c-4a49-a4e6-1e05776ed7e8";
    public static final String EXAMPLE_MODEL_NAME = "ExampleModel Name1";


    public static ExampleModel createModel(){
        ExampleModel model = new ExampleModel();
        model.setOrganizationId(ORGANIZATION_ID);
        model.setName(EXAMPLE_MODEL_NAME);
        return model;
    }


}
