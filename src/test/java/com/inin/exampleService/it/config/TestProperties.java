package com.inin.exampleService.it.config;

import com.inin.app.management.config.GlobalConfigKey;
import com.inin.app.management.properties.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestProperties
{
    public String getKafkaBrokerList() {
        return propertyService.getPropertyRegistry().getString(GlobalConfigKey.KafkaBrokerList.key());
    }

    private class Default {
        public static final String BASE_URI = "http://localhost:8070";
    }

    @Autowired
    private PropertyService propertyService;

    public String getLocalServiceBaseUri()
    {
        return propertyService.getPropertyRegistry().getString(TestConfigKey.LocalServiceBaseUri.key(), Default.BASE_URI);
    }

    public int getMockServerPort(){
        return propertyService.getPropertyRegistry().getInteger(TestConfigKey.MockServerPort.key(), 8070);
    }

    //Add other needed TestProperties
}
