package com.inin.exampleService.it.config;

import com.inin.app.management.config.ConfigKey;

public enum TestConfigKey implements ConfigKey
{

    LocalServiceBaseUri("it.localServiceBaseUrl"),
    MockServerPort("it.mockServerPort"),
    RedisServer("redis.server");
    
    private final String configKey;
    
    private TestConfigKey(String configKey){
        this.configKey = configKey;
    }
    
    @Override
    public String key()
    {
        return configKey;
    }

}
