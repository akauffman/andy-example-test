package com.inin.exampleService.it.config;

import com.inin.app.management.config.AppAdminConfig;
import com.inin.app.management.config.GlobalConfigKey;
import com.inin.app.management.properties.PropertyService;
import com.inin.management.admin.DynamoDbPropertyService;
import com.inin.exampleService.config.ExampleServiceConfigKey;
import com.inin.exampleService.config.ExampleServiceProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import java.io.IOException;


@Configuration
public class RootConfig
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(RootConfig.class);

    private PropertyService propertyService;
    private TestProperties testProperties;
    private ExampleServiceProperties serviceProperties;

    @Bean
    public PropertyService propertyService() throws IOException {
        if (propertyService == null) {

            AppAdminConfig
                    config = new AppAdminConfig.Builder(PropertyService.getEnvironment(), ExampleServiceConfigKey.ApplicationId)
                    .checkPropertyHasValue(GlobalConfigKey.KafkaBrokerList.key())
                    .checkPropertyHasValue(GlobalConfigKey.ZookeeperList.key())
                    .build();

            propertyService = new DynamoDbPropertyService(config.getPropertiesConfig());
            propertyService.initialize();
        }

        return propertyService;
    }
    
    @Bean
    public ExampleServiceProperties serviceProperties() throws IOException {

        if (serviceProperties == null) {
            System.setProperty("pc.platform.property.file", "src/test/resources/example-service.properties");
            System.setProperty("pc.it.dynamodb.endpoint", "http://localhost:12070");

            serviceProperties = new ExampleServiceProperties(this.propertyService());
        }
        return serviceProperties;
    }
    
    @Bean
    public TestProperties testProperties() throws IOException {

        if (testProperties == null) {
            testProperties = new TestProperties();
        }
        return testProperties;
    }

    @PreDestroy
    public void tearDown() {
        if (propertyService != null) {
            propertyService.shutdown();
        }
    }
}
