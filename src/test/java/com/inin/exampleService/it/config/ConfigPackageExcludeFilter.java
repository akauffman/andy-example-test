package com.inin.exampleService.it.config;

import org.springframework.core.type.filter.RegexPatternTypeFilter;

import java.util.regex.Pattern;

/**
 * Filter applied to the @ComponentScan to ignore the config package.  This prevents Spring from loading in the
 * configuration in config and re-creating the web app again.
 */
public class ConfigPackageExcludeFilter
        extends RegexPatternTypeFilter {
    public ConfigPackageExcludeFilter() {
        super(Pattern.compile("com\\.inin\\.exampleService\\.it\\.config\\..*"));
    }
}