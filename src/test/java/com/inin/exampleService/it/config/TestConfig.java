package com.inin.exampleService.it.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.mockserver.client.server.MockServerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.text.SimpleDateFormat;

@Configuration
@Import(RootConfig.class)
@EnableAspectJAutoProxy
@ComponentScan(basePackages = {"com.inin.exampleService.it"},
        excludeFilters = @ComponentScan.Filter(value = ConfigPackageExcludeFilter.class, type = FilterType.CUSTOM))
public class TestConfig
{

    private static final Logger LOGGER = LoggerFactory.getLogger(TestConfig.class);

    private MockServerClient mockServerClient;
    private ObjectMapper objectMapper;
    @Bean
    public ObjectMapper objectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            objectMapper.disable(SerializationFeature.WRITE_NULL_MAP_VALUES);
            objectMapper.enable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);
            objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX"));
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
        return objectMapper;
    }

    @Bean
    public MockServerClient mockServerClient(TestProperties properties) {
        if (mockServerClient == null) {
            int port = properties.getMockServerPort();
            mockServerClient = new MockServerClient("localhost", port);
        }
        return mockServerClient;
    }

    @PostConstruct
    public void initialize() throws IOException {

        //initialize here
    }

    @PreDestroy
    public void tearDown() {
    }

}
