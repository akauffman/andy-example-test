package com.inin.exampleService.it.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inin.exampleService.it.service.ExampleService;
import com.inin.logging.ContextLogger;
import com.inin.exampleService.config.ExampleServiceProperties;
import com.inin.exampleService.it.config.TestConfig;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockserver.client.server.MockServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.UUID;

@Category(org.springframework.boot.test.IntegrationTest.class)
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
public abstract class BaseIntegrationTest {

    @Autowired
    protected ExampleServiceProperties exampleServiceProperties;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected MockServerClient mockServer;

    @Autowired
    protected ExampleService exampleService;

    protected String userId;
    protected String correlationId;

    @Before
    public void baseSetup() {
        userId = UUID.randomUUID().toString();
        correlationId = UUID.randomUUID().toString();
        // ensure outgoing events have the correlationId
        new ContextLogger().withCorrelationId(correlationId);
    }

    @Before
    public synchronized void setup() {
        perClassSetup();
        mockServer.reset();
    }

    public void perClassSetup(){}
}
