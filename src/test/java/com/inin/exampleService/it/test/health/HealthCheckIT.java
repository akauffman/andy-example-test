package com.inin.exampleService.it.test.health;

import com.inin.contracts.app.management.HealthCheckPayload;
import com.inin.exampleService.it.test.BaseIntegrationTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class HealthCheckIT extends BaseIntegrationTest {

    @Test
    public void testHealthCheck(){
        HealthCheckPayload health = exampleService.getHealthCheck();

        Assert.assertNotNull("The service must provide a healthy health check", health);
        Assert.assertNotNull(health.getBuildJdk());
        Assert.assertNotNull(health.getImplementationBuild());
        Assert.assertNotNull(health.getImplementationVersion());
        Assert.assertNotNull(health.getBuiltBy());
        Assert.assertNotNull(health.getBuildTime());
        Assert.assertNotNull(health.getHealthy());
        if(StringUtils.equals(health.getBuiltBy(), "jenkins")) {
            Assert.assertNotNull(health.getBuildVersion());
        }
        Assert.assertEquals("exampleService", health.getRole());
    }
}
