package com.inin.exampleService.it;


import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.inin.app.management.config.GlobalConfigKey;
import com.inin.data.aws.dynamodb.util.DynamoDbDataStore;
import com.inin.data.aws.dynamodb.util.DynamoDbDataStoreImpl;
import com.inin.exampleService.it.config.TestConfigKey;
import com.inin.service.test.kafka.KafkaLocal;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.initialize.ExpectationInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * Integration Test Initialization Class/entry point.  Any global service startup initialization should happen below.
 */
public class App
        implements ExpectationInitializer
{
    private static final String PROPERTIES_GLOBAL = "properties-global";
    private static final String PROPERTIES_SERVICE = "properties-exampleService";

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    @Override
    public void initializeExpectations(MockServerClient mockServerClient)
    {

        // turn down logging for kafka
        System.setProperty("org.slf4j.simpleLogger.log.kafka", "error");
        System.setProperty("org.slf4j.simpleLogger.log.org.apache.zookeeper", "error");

        System.out.println("======================================\n");
        System.out.println("In example service Test App\n");
        System.out.println("======================================\n");

        try {
            createPropertyTables();
        } catch (Exception e) {
            System.out.println("error starting db!");
            e.printStackTrace();
            throw new RuntimeException("Boom: Unable to create property tables.");
        }

        try {
            Properties kafkaProperties = new Properties();
            Properties zkProperties = new Properties();

            //load properties
            System.out.println("Starting Kafka Server");

            File kafkaFile = new File("./src/test/resources/integrationtest-kafka.properties");
            if(!kafkaFile.exists())
            {//If running from the project root then we need to take into account the additional layer in the directory structure
                kafkaFile = new File("./exampleService/src/test/resources/integrationtest-kafka.properties");
            }
            try (InputStream is = new FileInputStream(kafkaFile)) {
                kafkaProperties.load(is);
            }

            File zkFile = new File("./src/test/resources/integrationtest-zookeeper.properties");
            if(!zkFile.exists())
            {
                zkFile = new File("./exampleService/src/test/resources/integrationtest-zookeeper.properties");
            }
            try (InputStream is = new FileInputStream(zkFile)) {
                zkProperties.load(is);
            }

            //start kafka
            final KafkaLocal kafkaLocal = new KafkaLocal(kafkaProperties, zkProperties);
            LOGGER.info("Initialized Kafka Server");

            Runtime runtime = Runtime.getRuntime();
            runtime.addShutdownHook(new Thread() {
                @Override
                public void run() {
                    LOGGER.info("Stopping Kafka Server");
                    kafkaLocal.stop();
                    LOGGER.info("Stopped Kafka Server");
                }
            });

            // give it time to really start...
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println("Error starting kafka - " + e.getMessage());

            LOGGER.info("Error starting Kafka Server!");
            e.printStackTrace();
            throw new RuntimeException("Error initializing Kafka", e);
        }
    }

    /**
     * Create the global and service property tables. We do this in the mock server so that everything is in place
     * before the example-service and tests are executed.  This allows them to connect to the proper kafka server
     * and the health check also does verification that required properties exist in the global properties table.
     */
    private void createPropertyTables()
    {
        AmazonDynamoDB client = new AmazonDynamoDBClient();
        client.setEndpoint("http://localhost:12070");

        DynamoDbDataStore dbDataStore = new DynamoDbDataStoreImpl(client, "");
        ArrayList<KeySchemaElement> ks = new ArrayList<>();
        ks.add(new KeySchemaElement().withAttributeName("key").withKeyType(KeyType.HASH));
        dbDataStore.createTable(PROPERTIES_GLOBAL, ks, 10, 5);

        List<HashMap<String, String>> listOfMaps = new ArrayList<>();
        addPropertyValue(listOfMaps, TestConfigKey.LocalServiceBaseUri.key(), "http://localhost:8070");
        addPropertyValue(listOfMaps, TestConfigKey.RedisServer.key(), "localhost");
        addPropertyValue(listOfMaps, GlobalConfigKey.KafkaBrokerList.key(), "127.0.0.1:10070");
        addPropertyValue(listOfMaps, GlobalConfigKey.ZookeeperList.key(), "localhost:11070");
        addPropertyValue(listOfMaps, GlobalConfigKey.Region.key(), "us-east-1");
        addPropertyValue(listOfMaps, GlobalConfigKey.FeatureTogglesBaseUri.key(), "feature-toggles.us-east-1.inindca.com/feature-toggles");

        dbDataStore.put(PROPERTIES_GLOBAL, listOfMaps);

        listOfMaps.clear();
        dbDataStore.createTable(PROPERTIES_SERVICE, ks, 10, 5);
        addPropertyValue(listOfMaps, "zookeeper.timeout", "10000");

        dbDataStore.put(PROPERTIES_SERVICE, listOfMaps);
        client.shutdown();
    }

    private static void addPropertyValue(List<HashMap<String, String>> values, String key, String value) {
        HashMap<String, String> item = new HashMap<>();
        item.put("key", key);
        item.put("rawKey", key);
        item.put("value", value);
        values.add(item);
    }
}
