package com.inin.exampleService.it.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inin.contracts.app.management.HealthCheckPayload;
import com.inin.exampleService.config.SharedObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExampleService {
    protected static final Logger LOGGER = LoggerFactory.getLogger(ExampleService.class);

    protected final static String BASE_URL = "http://localhost:8070";

    private RestTemplate restTemplate;

    @PostConstruct
    public void init(){

        ObjectMapper objectMapper = SharedObjectFactory.getObjectMapper();
        MappingJackson2HttpMessageConverter jacksonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jacksonHttpMessageConverter.setObjectMapper(objectMapper);
        List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
        converters.add(jacksonHttpMessageConverter);
        converters.add(new StringHttpMessageConverter());
        restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(converters);
    }

    public HealthCheckPayload getHealthCheck() {
        String url = BASE_URL + "/health/check";
        ResponseEntity<HealthCheckPayload> responseEntity = restTemplate.getForEntity(url, HealthCheckPayload.class);
        if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
            return responseEntity.getBody();
        }

        return null;
    }
}
