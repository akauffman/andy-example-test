package com.inin.exampleService.it.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inin.integration.kafka.configuration.KafkaPublisherConfig;
import com.inin.integration.kafka.notification.KafkaPublisher;
import com.inin.exampleService.config.SharedObjectFactory;
import com.inin.exampleService.it.config.TestProperties;
import org.mockserver.client.server.MockServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unchecked")
public abstract class BaseExampleService<T> {

    @Autowired
    protected MockServerClient mockServerClient;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected TestProperties properties;

    protected RestTemplate restTemplate;
    private KafkaPublisher kafkaPublisher;

    @PostConstruct
    public void initialize() {
        KafkaPublisherConfig pubConfig = new KafkaPublisherConfig(properties.getKafkaBrokerList(), 1);
        kafkaPublisher = new KafkaPublisher(pubConfig);

        ObjectMapper objectMapper = SharedObjectFactory.getObjectMapper();
        MappingJackson2HttpMessageConverter jacksonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jacksonHttpMessageConverter.setObjectMapper(objectMapper);
        List<HttpMessageConverter<?>> converters = Arrays.asList(jacksonHttpMessageConverter, new StringHttpMessageConverter());
        restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(converters);
    }

    @PreDestroy
    public void shutdown() {
        if (kafkaPublisher != null) {
            kafkaPublisher.shutDown();
        }
    }

    protected ResponseEntity<T> createEntity(Class<T> clazz, String resource, String organizationId, T entity) {
        try {

            String url = properties.getLocalServiceBaseUri() + resource;
            return restTemplate.postForEntity(url, entity, clazz, organizationId);

        } catch (HttpClientErrorException ex) {
            return new ResponseEntity<>(ex.getStatusCode());
        }
    }

    protected ResponseEntity<T> getEntity(Class<T> clazz, String resource, String organizationId, String id) {

        try {
            String url = properties.getLocalServiceBaseUri() + resource;
            return restTemplate.getForEntity(url, clazz, organizationId, id);

        } catch (HttpClientErrorException ex) {
            return new ResponseEntity<>(ex.getStatusCode());
        }
    }

    public ResponseEntity<T> deleteEntity(String resource, String organizationId, String id) {
        try {

            String url = properties.getLocalServiceBaseUri() + resource;
            restTemplate.delete(url, organizationId, id);
            return new ResponseEntity(id, HttpStatus.OK);

        } catch (HttpClientErrorException ex) {
            return new ResponseEntity<>(ex.getStatusCode());
        }
    }

    public ResponseEntity<T> updateEntity(Class<T> clazz, String resource, String organizationId, T entity) {
        try {

            String url = properties.getLocalServiceBaseUri() + resource;
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<T> httpEntity = new HttpEntity<>(entity, httpHeaders);
            return restTemplate.exchange(url, HttpMethod.PUT, httpEntity, clazz, organizationId);

        } catch (HttpClientErrorException ex) {
            return new ResponseEntity<>(ex.getStatusCode());
        }
    }
}
