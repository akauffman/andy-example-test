# PureCloud Example Service 2016

## Git Usage
-----

All commits to the example-service repository must contain a JIRA issue number or [NO-JIRA] in the commit message. This makes it easy to track what changes went into what release, and track when JIRA issues are resolved. To make this easier to manage there are some git hooks in the git_hooks directory that will automatically add the git branch name to the commit messages and check that the commit message has a JIRA issue in it. As long as you name your git branches after the JIRA issue, then you won't have to do anything else. To install the git hooks simply run

>  git_hooks/install-hooks.sh

from the root of the project.

## Building
-----

From the root of the project, run

>    mvn package

## Running
----

To run the service locally, make sure you have access to the OpenVPN and that it is connected.

    If you do not have access to it, submit a devops ticket here: https://inindca.atlassian.net/servicedesk/customer/portal/2

Two files need to be in your /etc directory

    inin.conf
    service.properties

The first file points to  the service properties file and the service properties file details which dependent services should be run locally and which dependent service should be run remotely.


### JVM Options

make sure you have a local environment variable set as follows:

    environment = dev
    This can be set via command line args to maven with "-Denvironment=dev"
    Or by adding "export ENVIRONMENT=dev" in your shell environment as described above

Then run the command

    mvn spring-boot:run -Drun.jvmArguments="-Denvironment=dev" OR
    java -Dserver.port:8098 -jar target/exampleService.jar OR
    ./run.sh

By default, the service endpoint will be:

    http://localhost:8098/exampleService

## Unit Tests

To run the tests

    mvn test

## Integration Tests

#### Prerequisites
   - wget
     - Required to set up the Redis cluster
     - You probably already have this installed.  If not, use your favorite package manager.

#### To run the tests

    mvn clean verify

It is also possible to connect a debugger to the above mentioned command. If you want to monitor the actual service test,
run the following command:

    mvn clean verify -P it-debug-test

If you want to monitor the service while the service tests are running,
run this command and connect you debugger to port 40000

    mvn clean verify -P it-debug-svc

You can also run a single test like this.

    mvn clean verify -DfailIfNoTests=false -Dit.test=TestClassIT#testMethod

You can do the same in IntelliJ by running this maven configuration

    clean verify -DforkMode=never -DforkCount=1 -DfailIfNoTests=false -Dit.test=TestClassIT#testMethod


## Configured Dependencies - service has infra files for the following dependencies, it mainly uses dynamodb to demonstrate as an example

-----

  *    Redis - redis.server, redis.port     

  *    DynamoDB - Used through EntityManager

  *    Amazon S3

## Technology
-----

  *    [Spring IoC](http://docs.spring.io/spring/docs/2.0.8/reference/beans.html) - Inversion of Control framework
  *    [Spring MVC](http://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/mvc.html) - Web application framework
  *    [JUnit](http://junit.org/) - Unit testing framework
  *    [Mockito](https://code.google.com/p/mockito/) - Mocking framework
  *    [Logback](http://logback.qos.ch/) - Logging framework
