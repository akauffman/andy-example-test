
package com.inin.schema.exampleService.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * ErrorInfo
 * <p>
 * Error information that the Public API will receive in a response body. This allows backend services to pass an error message to consumers of the Public API.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "errorCode",
    "status",
    "userMessage",
    "userParamsMessage",
    "userParams"
})
public class ErrorInfo {

    /**
     * A code unique to this error. Typically prefixed with the service that originated the error. For example CONFIG_USER_NOT_FOUND
     * 
     */
    @JsonProperty("errorCode")
    private String errorCode;
    /**
     * The HTTP status code for this message. If left blank the status code from the HTTP response is used.
     * 
     */
    @JsonProperty("status")
    private Integer status;
    /**
     * A customer friendly message. This should be a complete sentence, use proper grammar and only include information useful to a customer. This is not a dev message and should not include things like Org Id
     * 
     */
    @JsonProperty("userMessage")
    private String userMessage;
    /**
     * This is the same as userMessage except it uses template fields for variable replacement. For instance: 'User {username} was not found'
     * 
     */
    @JsonProperty("userParamsMessage")
    private String userParamsMessage;
    /**
     * Used in conjunction with userParamsMessage. These are the template parameters. For instance: UserParam.key = 'username', UserParam.value = 'chuck.pulfer'
     * 
     */
    @JsonProperty("userParams")
    private List<UserParam> userParams = new ArrayList<UserParam>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * A code unique to this error. Typically prefixed with the service that originated the error. For example CONFIG_USER_NOT_FOUND
     * 
     */
    @JsonProperty("errorCode")
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * A code unique to this error. Typically prefixed with the service that originated the error. For example CONFIG_USER_NOT_FOUND
     * 
     */
    @JsonProperty("errorCode")
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorInfo withErrorCode(String errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    /**
     * The HTTP status code for this message. If left blank the status code from the HTTP response is used.
     * 
     */
    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    /**
     * The HTTP status code for this message. If left blank the status code from the HTTP response is used.
     * 
     */
    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    public ErrorInfo withStatus(Integer status) {
        this.status = status;
        return this;
    }

    /**
     * A customer friendly message. This should be a complete sentence, use proper grammar and only include information useful to a customer. This is not a dev message and should not include things like Org Id
     * 
     */
    @JsonProperty("userMessage")
    public String getUserMessage() {
        return userMessage;
    }

    /**
     * A customer friendly message. This should be a complete sentence, use proper grammar and only include information useful to a customer. This is not a dev message and should not include things like Org Id
     * 
     */
    @JsonProperty("userMessage")
    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public ErrorInfo withUserMessage(String userMessage) {
        this.userMessage = userMessage;
        return this;
    }

    /**
     * This is the same as userMessage except it uses template fields for variable replacement. For instance: 'User {username} was not found'
     * 
     */
    @JsonProperty("userParamsMessage")
    public String getUserParamsMessage() {
        return userParamsMessage;
    }

    /**
     * This is the same as userMessage except it uses template fields for variable replacement. For instance: 'User {username} was not found'
     * 
     */
    @JsonProperty("userParamsMessage")
    public void setUserParamsMessage(String userParamsMessage) {
        this.userParamsMessage = userParamsMessage;
    }

    public ErrorInfo withUserParamsMessage(String userParamsMessage) {
        this.userParamsMessage = userParamsMessage;
        return this;
    }

    /**
     * Used in conjunction with userParamsMessage. These are the template parameters. For instance: UserParam.key = 'username', UserParam.value = 'chuck.pulfer'
     * 
     */
    @JsonProperty("userParams")
    public List<UserParam> getUserParams() {
        return userParams;
    }

    /**
     * Used in conjunction with userParamsMessage. These are the template parameters. For instance: UserParam.key = 'username', UserParam.value = 'chuck.pulfer'
     * 
     */
    @JsonProperty("userParams")
    public void setUserParams(List<UserParam> userParams) {
        this.userParams = userParams;
    }

    public ErrorInfo withUserParams(List<UserParam> userParams) {
        this.userParams = userParams;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
